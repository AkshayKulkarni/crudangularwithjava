import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private userService: UserServiceService) { }
  userDetails;
  userForm = new FormGroup({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    emailId: new FormControl(''),
    password: new FormControl('')
  });
  ngOnInit() {
  }
  addUserDetails() {
    this.userDetails = this.userForm.value;
    console.log(this.userForm.value);
    this.userService.sendUserDetails(this.userDetails).subscribe((msg)=>{console.log(msg); });
  }

}
