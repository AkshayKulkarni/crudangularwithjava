import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }
  sendUserDetails(userDetails) {
    console.log(userDetails);
    return this.http.post("http://localhost:8080/CrudOperation/AddServlet", userDetails);
  }
  viewUserDetails() {
   return this.http.get("http://localhost:8080/CrudOperation/ViewServlet");
  }
  deleteUser(i) {
    return this.http.post("http://localhost:8080/CrudOperation/DeleteServlet",i);
  }
  editUser(i){
    return this.http.post("http://localhost:8080/CrudOperation/EditServlet",i);
  }
}
