import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../services/user-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  constructor(private userservice: UserServiceService, private router: Router) { }
  responseData: any[]=[];
  tempObj: any = {};
  res: any = {};
  userDetails;
  userForm = new FormGroup({
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    emailId: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });
  ngOnInit() {
    this.view();
  }
  view(){
    this.userservice.viewUserDetails().subscribe((r:any)=>this.responseData=r);
  }
  addUserDetails() {
    this.userDetails = this.userForm.value;
    console.log(this.userForm.value);
    this.userservice.sendUserDetails(this.userDetails).subscribe((msg)=>{
      console.log(msg);
      if (msg == 'done') { 
        alert('Add new user successfully');
        this.router.navigate(['/']);
      }});
  }
  DeleteUser(i) {
    this.tempObj.id = this.responseData[i].id;
    this.userservice.deleteUser(this.tempObj).subscribe((response:any)=>{
      this.res = response;
      if (this.res.msg=="done") {
        alert("deleted successfully");
        this.view();
      } else {
        alert("Sorry Unable to delete Data");
      }
    });
  }
}
