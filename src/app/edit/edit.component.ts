import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor(private act: ActivatedRoute, private rr: Router, private userService: UserServiceService) { }
  id:number;
  user: any[] = [];
  list: any = {};
  ngOnInit() {
    this.id = this.act.snapshot.params['i'];
    this.userService.viewUserDetails().subscribe((d: any) =>{ 
      this.user = d;
      for(let i = 0; i < this.user.length; i++) {
        if(i == this.id) {
          this.list = this.user[i];
          break;
        }
      }
    });
  }

  update() {
    this.userService.editUser(this.list).subscribe(d=>{
        alert("updated successfully");
        this.rr.navigate(['/']);
      });
  }

}
