import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ViewComponent } from './view/view.component';
import {EditComponent} from './edit/edit.component';

const routes: Routes = [
  {path: '', component: ViewComponent},
  {path: 'home', component: HomeComponent},
  {path:'edit/:i', component:EditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
